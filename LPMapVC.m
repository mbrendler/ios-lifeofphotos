//
//  LPMapVCViewController.m
//  LifeOfPhotos
//
//  Created by Maik Brendler on 31.07.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import "LPMapVC.h"
#import "LPPlaceMapAnnotation.h"
#import "LPPhotoMapAnnotation.h"
#import <MapKit/MapKit.h>

@interface LPMapVC () <MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
- (IBAction)changeMapType:(UISegmentedControl *)sender;
@end

@implementation LPMapVC
@synthesize mapView = _mapView;
@synthesize delegate = _delegate;

- (IBAction)changeMapType:(UISegmentedControl *)sender
{
    // The segment indexes are compatible with the enum MKMapType:
    self.mapView.mapType = sender.selectedSegmentIndex;
}

#pragma mark - Synchronize Model and View

- (void)updateMapView
{
    if (self.mapView.annotations) {
        [self.mapView removeAnnotations:self.mapView.annotations];
    }
    if (self.annotations) {
        [self.mapView addAnnotations:self.annotations];
    }
    
    CLLocationDegrees minLatitude = 360;
    CLLocationDegrees maxLatitude = -360;
    CLLocationDegrees minLongitude = 360;
    CLLocationDegrees maxLongitude = -360;
    for (id<MKAnnotation> annotation in self.annotations) {
        CLLocationCoordinate2D coordinate = [annotation coordinate];
        if (minLatitude > coordinate.latitude) {
            minLatitude = coordinate.latitude;
        } else if (maxLatitude < coordinate.latitude) {
            maxLatitude = coordinate.latitude;
        }
        if (minLongitude > coordinate.longitude) {
            minLongitude = coordinate.longitude;
        } else if (maxLongitude < coordinate.longitude) {
            maxLongitude = coordinate.longitude;
        }
    }
    
    MKCoordinateRegion region = MKCoordinateRegionMake(
        CLLocationCoordinate2DMake(
                (minLatitude + maxLatitude) / 2,
                (minLongitude + maxLongitude) / 2),
        MKCoordinateSpanMake(
                maxLatitude - minLatitude,
                maxLongitude - minLongitude));
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:NO];
}

- (void)setMapView:(MKMapView *)mapView
{
    _mapView = mapView;
    [self updateMapView];
}

- (void)setAnnotations:(NSArray *)annotations
{
    _annotations = annotations;
    [self updateMapView];
}

#pragma mark - MapViewDelegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    MKAnnotationView *aView = [mapView dequeueReusableAnnotationViewWithIdentifier:@"MapVC"];
    if (!aView) {
        aView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"MapVC"];
        aView.canShowCallout = YES;
        aView.leftCalloutAccessoryView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        aView.rightCalloutAccessoryView = button;
    }
    
    aView.annotation = annotation;
    [(UIImageView *)aView.leftCalloutAccessoryView setImage:nil];
    
    return aView;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)aView
{
    dispatch_queue_t downloadQueue = dispatch_queue_create(
                                            "image downloader", NULL);
    dispatch_async(downloadQueue, ^{
        id annotation = aView.annotation;
        UIImage *image = [self.delegate mapViewController:self
                                       imageForAnnotation:aView.annotation];
        if (aView.annotation != annotation) return;
        dispatch_async(dispatch_get_current_queue(), ^{
            [(UIImageView *)aView.leftCalloutAccessoryView setImage:image];
        });
    });
    dispatch_release(downloadQueue);
}

- (void)mapView:(MKMapView *)mapView
 annotationView:(MKAnnotationView *)view
calloutAccessoryControlTapped:(UIControl *)control
{
    [self.delegate mapViewController:self annotationClicked:view.annotation];
}

#pragma mark - View Controller Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.mapView.delegate = self;
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [self setMapView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

@end
