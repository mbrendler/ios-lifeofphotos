//
//  main.m
//  LifeOfPhotos
//
//  Created by Maik Brendler on 27.07.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LPAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LPAppDelegate class]));
    }
}
