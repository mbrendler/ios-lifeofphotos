//
//  LPBaseTVC.m
//  LifeOfPhotos
//
//  Created by Maik Brendler on 27.07.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import "LPBaseTVC.h"
#import "LPMapVC.h"
#import <MapKit/MapKit.h>

@interface LPBaseTVC () <LPMapVCDelegate>
@property (nonatomic) BOOL loadsData;
@end

@implementation LPBaseTVC
@synthesize tableData = _tableData;

- (NSArray*)loadTableDataInDownloadQueue
{
    // This method is called on loading data in a background thread.
    // Return an array of tableData.
    return nil;
}

- (char*)backgroundThreadName
{
    static char *name = "flickr downloader";
    return name;
}

- (void)loadTableData
{
    if (self.loadsData) return;
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [spinner startAnimating];
    UIBarButtonItem *rightBarButton = self.navigationItem.rightBarButtonItem;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                              initWithCustomView:spinner];
    
    self.loadsData = YES;
    dispatch_queue_t downloadQueue = dispatch_queue_create(
                                            [self backgroundThreadName], NULL);
    dispatch_async(downloadQueue, ^{
        NSArray *tableData = [self loadTableDataInDownloadQueue];
        dispatch_async(dispatch_get_current_queue(), ^{
            self.navigationItem.rightBarButtonItem = rightBarButton;
            self.tableData = tableData;
            self.loadsData = NO;
        });
    });
    dispatch_release(downloadQueue);
}

- (void)setTableData:(NSArray *)tableData
{
    if (tableData != _tableData) {
        _tableData = tableData;
        [self.tableView reloadData];
    }
}

#pragma mark - Map Annotation

+ (id<MKAnnotation>)constructAnnotation:(id)tableDataElement
{
    // Overwrite this method an construct a annotation element here.
    return nil;
}

- (NSArray*)annotations
{
    NSMutableArray *annotations = [NSMutableArray
                                   arrayWithCapacity:[self.tableData count]];
    for (id tableDataElement in self.tableData) {
        id<MKAnnotation> annotation = [[self class]
                                       constructAnnotation:tableDataElement];
        if (annotation) [annotations addObject:annotation];
    }
    return annotations;
}

- (UIImage *)mapViewController:(LPMapVC*)sender
            imageForAnnotation:(id <MKAnnotation>)annotation
{
    return nil;
}

- (void)mapViewController:(LPMapVC*)sender
        annotationClicked:(id <MKAnnotation>)annotation
{
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Show Map"]) {
        [segue.destinationViewController setDelegate:self];
        [segue.destinationViewController setAnnotations:[self annotations]];
    }
}

#pragma mark - Controller Live Cycle

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadTableData];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [self.tableData count];
}

- (void)fillCell:(UITableViewCell*)cell withCellData:(id)cellData
{
    // overwrite this method to fill the given cell with the given data.
    cell.textLabel.text = [cellData description];
}

- (NSString*)cellIdentifier
{
    return @"Cell";
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             self.cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:self.cellIdentifier];
    }
    
    id cellData = [self.tableData objectAtIndex:indexPath.row];
    [self fillCell:cell withCellData:cellData];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
