//
//  LPPhotoMapAnnotation.h
//  LifeOfPhotos
//
//  Created by Maik Brendler on 31.07.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface LPPhotoMapAnnotation : NSObject <MKAnnotation>

+(LPPhotoMapAnnotation*)annotationForPhoto:(NSDictionary*)photo;

@property (nonatomic,strong) NSDictionary* photo;

@end
