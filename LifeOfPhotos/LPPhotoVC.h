//
//  LPPhotoVC.h
//  LifeOfPhotos
//
//  Created by Maik Brendler on 27.07.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SplitViewBarButtonItemPresenter.h"

#define RECENT_PHOTOS_KEY @"RecentPhotos"

@interface LPPhotoVC : UIViewController <SplitViewBarButtonItemPresenter>

@property (nonatomic, strong) NSDictionary* photoInfo;

+ (NSString*)photoTitleByInfo:(NSDictionary*)photoInfo;

@end
