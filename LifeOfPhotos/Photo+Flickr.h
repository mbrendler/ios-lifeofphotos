//
//  Photo+Flickr.h
//  LifeOfPhotos
//
//  Created by Maik Brendler on 07.08.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import "Photo.h"

@interface Photo (Flickr)

+ (BOOL)exist:(NSDictionary*)photoInfo
inManagedObjectContext:(NSManagedObjectContext*)context;

+ (Photo*)get:(NSDictionary*)photoInfo
fromManagedObjectContext:(NSManagedObjectContext*)context;

+ (Photo*)create:(NSDictionary*)photoInfo
inManagedObjectContext:(NSManagedObjectContext*)context;

+ (void)remove:(NSDictionary*)photoInfo
fromManagedObjectContext:(NSManagedObjectContext*)context;

- (NSDictionary*)flickrPhotoInfo;

@end
