//
//  LPVacationTagTVC.m
//  LifeOfPhotos
//
//  Created by Maik Brendler on 09.08.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import "LPVacationTagTVC.h"
#import "LPVacationData.h"
#import "Tag.h"
#import "LPVacationPhotosTVC.h"

@interface LPVacationTagTVC ()

@property (nonatomic,strong) UIManagedDocument *vacation;

@end

@implementation LPVacationTagTVC
@synthesize vacationName = _vacationName;

- (void)setupFetchedResultsController
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Tag"];
    request.sortDescriptors = @[
        [NSSortDescriptor sortDescriptorWithKey:@"photoCount" ascending:NO],
        [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES],
    ];
    
    [LPVacationData
     openVacation:self.vacationName
     usingBlock:^(UIManagedDocument *vacation){
         self.fetchedResultsController = [[NSFetchedResultsController alloc]
                                          initWithFetchRequest:request
                                          managedObjectContext:vacation.managedObjectContext
                                          sectionNameKeyPath:nil
                                          cacheName:nil];
     }];
}

- (void)setVacationName:(NSString *)vacationName
{
    if (vacationName != _vacationName) {
        _vacationName = vacationName;
        [self setupFetchedResultsController];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Vacation Tag Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    Tag *tag = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = [tag.name capitalizedString];
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Show Vacation Table"]) {
        LPVacationPhotosTVC *photosTvc = segue.destinationViewController;
        photosTvc.vacationName = self.vacationName;
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        photosTvc.tag = [self.fetchedResultsController objectAtIndexPath:indexPath];
    }
}

@end
