//
//  LPPhotoCache.m
//  LifeOfPhotos
//
//  Created by Maik Brendler on 31.07.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import "LPCache.h"

NSMutableDictionary *caches = nil;

@interface LPCache ()
@property (nonatomic,strong) NSString* facility;
@property (nonatomic) filesize_t maxSizeInBytes;

@property (readonly,strong) NSURL* cacheUrl;
@end


@implementation LPCache
@synthesize facility = _facility;
@synthesize maxSizeInBytes = _maxSizeInBytes;

+ (LPCache*)cacheForFacility:(NSString*)facility
           andMaxSizeInBytes:(filesize_t)maxSize
{
    if (nil == caches) {
        caches = [[NSMutableDictionary alloc] init];
    }
    LPCache *result = [caches objectForKey:facility];
    if (nil == result) {
        result = [[LPCache alloc] init];
        result.facility = facility;
        result.maxSizeInBytes = maxSize;
    }
    return result;
}

- (NSURL*)cacheUrl
{
    NSFileManager *filemanager = [NSFileManager defaultManager];
        NSURL *cacheBaseUrl = [filemanager URLForDirectory:NSCachesDirectory
                                                  inDomain:NSUserDomainMask
                                         appropriateForURL:nil
                                                    create:YES
                                                     error:nil];
    NSURL *cacheUrl = [cacheBaseUrl URLByAppendingPathComponent:self.facility];
    
    // Create the caching directory if it does not exist
    [filemanager createDirectoryAtURL:cacheUrl
          withIntermediateDirectories:YES
                           attributes:nil
                                error:nil];
    return cacheUrl;
}

- (NSURL*)fileUrlForKey:(NSString*)key
{
    return [self.cacheUrl URLByAppendingPathComponent:key];
}

- (void)limitCacheSize
{
    NSFileManager *filemanager = [NSFileManager defaultManager];
    
    NSArray *keys = @[NSURLAttributeModificationDateKey, NSURLFileSizeKey];
    NSArray *cachedFiles = [filemanager contentsOfDirectoryAtURL:self.cacheUrl
                                      includingPropertiesForKeys:keys
                                                         options:0
                                                           error:nil];
    
    filesize_t size = 0;
    for (NSURL* cachedFile in cachedFiles) {
        NSNumber *fileSize = nil;
        [cachedFile getResourceValue:&fileSize forKey:NSURLFileSizeKey error:nil];
        size += [fileSize unsignedLongLongValue];
    }
    if (self.maxSizeInBytes < size) {
        cachedFiles = [cachedFiles sortedArrayUsingComparator:
                       ^NSComparisonResult(id a, id b) {
                           NSDate *first = nil;
                           [(NSURL*)a getResourceValue:&first
                                                forKey:NSURLAttributeModificationDateKey
                                                 error:nil];
                           NSDate *second = nil;
                           [(NSURL*)b getResourceValue:&second
                                                forKey:NSURLAttributeModificationDateKey
                                                 error:nil];
                           return [first compare:second];
                       }];
        for (NSURL* cachedFile in cachedFiles) {
            [filemanager removeItemAtURL:cachedFile error:nil];
            NSNumber *fileSize = nil;
            [cachedFile getResourceValue:&fileSize forKey:NSURLFileSizeKey error:nil];
            size -= [fileSize unsignedLongLongValue];
            if (self.maxSizeInBytes >= size) break;
        }
    }
}

- (void)add:(NSData*)data forKey:(NSString*)key
{
    if (!data) return;
    // TODO: if ([LPPhotoCache photoForKey:key]) return; // is allready cached.
    
    NSFileManager *filemanager = [NSFileManager defaultManager];
    [filemanager createFileAtPath:[[self fileUrlForKey:key] path]
                         contents:data
                       attributes:nil];
    
}

- (NSData*)dataForKey:(NSString*)key
{
    return [NSData dataWithContentsOfURL:[self fileUrlForKey:key]];
}

@end
