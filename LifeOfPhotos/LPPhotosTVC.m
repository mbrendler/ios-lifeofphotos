//
//  LPPhotosTVC.m
//  LifeOfPhotos
//
//  Created by Maik Brendler on 27.07.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import "LPPhotosTVC.h"
#import "FlickrFetcher.h"
#import "LPPhotoVC.h"
#import "LPPhotoMapAnnotation.h"
#import "LPMapVC.h"
#import <MapKit/MapKit.h>

@interface LPPhotosTVC ()

@end

@implementation LPPhotosTVC

- (NSArray*)loadTableDataInDownloadQueue
{
    return [FlickrFetcher photosInPlace:self.place maxResults:50];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = [self.place objectForKey:FLICKR_PLACE_NAME];
}

+ (id<MKAnnotation>)constructAnnotation:(id)tableDataElement
{
    return [LPPhotoMapAnnotation annotationForPhoto:tableDataElement];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [super prepareForSegue:segue sender:sender];
    if ([segue.identifier isEqualToString:@"Show Photo"]) {
        NSDictionary *photo = nil;
        if ([sender isKindOfClass:[LPPhotoMapAnnotation class]]) {
            photo = [sender photo];
        } else {
            NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
            photo = [self.tableData objectAtIndex:indexPath.row];
        }
        [segue.destinationViewController setPhotoInfo:photo];
    }
}

- (UIImage *)mapViewController:(LPMapVC*)sender
            imageForAnnotation:(LPPhotoMapAnnotation*)annotation
{
    NSURL *url = [FlickrFetcher urlForPhoto:annotation.photo
                                     format:FlickrPhotoFormatSquare];
    NSData *data = [NSData dataWithContentsOfURL:url];
    return data ? [UIImage imageWithData:data] : nil;
}

- (void)mapViewController:(LPMapVC*)sender
        annotationClicked:(LPPhotoMapAnnotation*)annotation
{
    if (self.splitViewController) {
        LPPhotoVC *photoViewController = [self.splitViewController.viewControllers lastObject];
        photoViewController.photoInfo = annotation.photo;
    } else {
        [self performSegueWithIdentifier:@"Show Photo" sender:annotation];
    }
}

- (NSString*)cellIdentifier
{
    return @"Photo Table Cell";
}

- (void)fillCell:(UITableViewCell*)cell withCellData:(NSDictionary*)cellData
{
    NSString *labelText = [LPPhotoVC photoTitleByInfo:cellData];
    cell.textLabel.text = labelText;
    cell.detailTextLabel.text = [cellData valueForKeyPath:FLICKR_PHOTO_DESCRIPTION];
    // Clear the image because the cells will be reused, so do not show images
    // on incorrect cells.
    cell.imageView.image = nil;
    [cell.imageView setContentMode:UIViewContentModeScaleAspectFill];
    dispatch_queue_t downloadQueue = dispatch_queue_create(
                                        "small downloader", NULL);
    dispatch_async(downloadQueue, ^{
        NSURL *imageUrl = [FlickrFetcher urlForPhoto:cellData
                                              format:FlickrPhotoFormatSquare];
        NSData *imageData = [NSData dataWithContentsOfURL:imageUrl];
        if (![cell.textLabel.text isEqualToString:labelText]) return;
        dispatch_async(dispatch_get_main_queue(), ^{
            cell.imageView.image = [UIImage imageWithData:imageData];
            [cell setNeedsDisplay];
        });
    });
    dispatch_release(downloadQueue);
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.splitViewController) {
        LPPhotoVC *photoViewController = [self.splitViewController.viewControllers objectAtIndex:1];
        NSDictionary *photo = [self.tableData objectAtIndex:indexPath.row];
        photoViewController.photoInfo = photo;
    }
}

@end
