//
//  LPPlaceMapAnnotation.m
//  LifeOfPhotos
//
//  Created by Maik Brendler on 31.07.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import "LPPlaceMapAnnotation.h"
#import "FlickrFetcher.h"

@implementation LPPlaceMapAnnotation
@synthesize place = _place;

+(LPPlaceMapAnnotation*)annotationForPlace:(NSDictionary*)place
{
    LPPlaceMapAnnotation *annotation = [[LPPlaceMapAnnotation alloc] init];
    annotation.place = place;
    return annotation;
}

#pragma mark - MKAnnotation

- (NSString *)title
{
    return [self.place objectForKey:FLICKR_PLACE_NAME];
}

- (CLLocationCoordinate2D)coordinate
{
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = [[self.place objectForKey:FLICKR_LATITUDE] doubleValue];
    coordinate.longitude = [[self.place objectForKey:FLICKR_LONGITUDE] doubleValue];
    return coordinate;
}

@end
