//
//  LPTopPlacesTVC.m
//  LifeOfPhotos
//
//  Created by Maik Brendler on 27.07.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import "LPTopPlacesTVC.h"
#import "FlickrFetcher.h"
#import "LPPhotosTVC.h"
#import "LPPlaceMapAnnotation.h"
#import "LPMapVC.h"
#import <MapKit/MapKit.h>

@interface LPTopPlacesTVC ()

@end

@implementation LPTopPlacesTVC

- (NSArray*)loadTableDataInDownloadQueue
{
    NSArray *places = [FlickrFetcher topPlaces];
    return [places sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSString *first = [(NSDictionary*)a objectForKey:FLICKR_PLACE_NAME];
        NSString *second = [(NSDictionary*)b objectForKey:FLICKR_PLACE_NAME];
        return [first compare:second];
    }];
}

+ (id<MKAnnotation>)constructAnnotation:(id)tableDataElement
{
    return [LPPlaceMapAnnotation annotationForPlace:tableDataElement];
}

- (void)mapViewController:(LPMapVC*)sender
        annotationClicked:(LPPlaceMapAnnotation*)annotation
{
    [self performSegueWithIdentifier:@"Place Photo Table" sender:annotation];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [super prepareForSegue:segue sender:sender];
    if ([segue.identifier isEqualToString:@"Place Photo Table"]) {
        NSDictionary *place = nil;
        if ([sender isKindOfClass:[LPPlaceMapAnnotation class]]) {
            place = [sender place];
        } else {
            NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
            place = [self.tableData objectAtIndex:indexPath.row];
        }
        [segue.destinationViewController setPlace:place];
    }
}

- (void)fillCell:(UITableViewCell*)cell withCellData:(NSDictionary*)cellData
{
    NSString *place = [cellData objectForKey:FLICKR_PLACE_NAME];
    NSRange splitIndex = [place rangeOfString:@", "];
    cell.textLabel.text = [place substringToIndex:splitIndex.location];
    cell.detailTextLabel.text = [place substringFromIndex:
                                 splitIndex.location + splitIndex.length];
}

- (NSString*)cellIdentifier
{
    return @"Top Place Table Cell";
}

@end
