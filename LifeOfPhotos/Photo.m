//
//  Photo.m
//  LifeOfPhotos
//
//  Created by Maik Brendler on 07.08.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import "Photo.h"
#import "Place.h"
#import "Tag.h"


@implementation Photo

@dynamic title;
@dynamic subtitle;
@dynamic unique;
@dynamic createDate;
@dynamic farm;
@dynamic server;
@dynamic secret;
@dynamic originalSecret;
@dynamic tags;
@dynamic place;

@end
