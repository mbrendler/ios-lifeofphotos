//
//  Tag+Create.h
//  LifeOfPhotos
//
//  Created by Maik Brendler on 09.08.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import "Tag.h"

@interface Tag (Create)

+ (Tag*)create:(NSString*)name
inManagedObjectContext:(NSManagedObjectContext*)context;

+ (NSSet*)createTags:(NSArray*)names
inManagedObjectContext:(NSManagedObjectContext*)context;

@end
