//
//  LPVacationTagTVC.h
//  LifeOfPhotos
//
//  Created by Maik Brendler on 09.08.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataTableViewController.h"

@interface LPVacationTagTVC : CoreDataTableViewController

@property (nonatomic,strong) NSString *vacationName;

@end
