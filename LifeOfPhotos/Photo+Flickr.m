//
//  Photo+Flickr.m
//  LifeOfPhotos
//
//  Created by Maik Brendler on 07.08.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import "Photo+Flickr.h"
#import "FlickrFetcher.h"
#import "Place+Create.h"
#import "Tag+Create.h"

@implementation Photo (Flickr)

+ (BOOL)exist:(NSDictionary*)photoInfo
inManagedObjectContext:(NSManagedObjectContext*)context
{
    return nil != [self get:photoInfo fromManagedObjectContext:context];
}

+ (Photo*)get:(NSDictionary*)photoInfo
fromManagedObjectContext:(NSManagedObjectContext*)context
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Photo"];
    NSString *photoId = [photoInfo objectForKey:FLICKR_PHOTO_ID];
    request.predicate = [NSPredicate predicateWithFormat:@"unique = %@", photoId];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor
                                        sortDescriptorWithKey:@"createDate"
                                        ascending:YES];
    request.sortDescriptors = @[sortDescriptor];
    
    NSError *error = [[NSError alloc] init];
    NSArray *photos = [context executeFetchRequest:request error:&error];
    return [photos lastObject];
}

+ (Photo*)create:(NSDictionary*)photoInfo
inManagedObjectContext:(NSManagedObjectContext*)context
{
    Photo *photo = [self get:photoInfo fromManagedObjectContext:context];
    if (!photo) {
        photo = [NSEntityDescription insertNewObjectForEntityForName:@"Photo"
                                              inManagedObjectContext:context];
        photo.unique = [photoInfo objectForKey:FLICKR_PHOTO_ID];
        photo.title = [photoInfo objectForKey:FLICKR_PHOTO_TITLE];
        photo.subtitle = [photoInfo valueForKeyPath:FLICKR_PHOTO_DESCRIPTION];
        photo.farm = [photoInfo objectForKey:@"farm"];
        photo.server = [photoInfo objectForKey:@"server"];
        photo.secret = [photoInfo objectForKey:@"secret"];
        photo.originalSecret = [photoInfo objectForKey:@"originalsecret"];
        photo.createDate = [NSDate date];
        NSArray *tagNames = [[photoInfo objectForKey:FLICKR_TAGS]
                             componentsSeparatedByString:@" "];
        [photo addTags:
         [Tag createTags:tagNames inManagedObjectContext:context]];
        
        photo.place = [Place
                       create:[photoInfo objectForKey:FLICKR_PHOTO_PLACE_NAME]
                       inManagedObjectContext:context];
        for (Tag *tag in photo.tags) {
            tag.photoCount = [NSNumber numberWithInt:[tag.photos count]];
        }
    }
    return photo;
}

+ (void)remove:(NSDictionary*)photoInfo
fromManagedObjectContext:(NSManagedObjectContext*)context
{
    Photo *photo = [self get:photoInfo fromManagedObjectContext:context];
    if (photo) {
        NSSet *tags = photo.tags;
        Place *place = photo.place;
        for (Tag *tag in tags) {
            NSInteger photoCountOfTag = [tag.photos count] - 1;
            tag.photoCount = [NSNumber numberWithInt:photoCountOfTag];
            if (0 == photoCountOfTag) {
                [context deleteObject:tag];
            }
        }
        if (1 == [place.photos count]) {
            [context deleteObject:place];
        }
        [context deleteObject:photo];
    }
}

- (NSDictionary*)flickrPhotoInfo
{
    return @{
        FLICKR_PHOTO_ID: self.unique,
        FLICKR_PHOTO_TITLE: self.title,
        @"farm": self.farm,
        @"server": self.server,
        @"secret": self.secret,
        //@"originalsecret": self.originalSecret,
    };
}

@end
