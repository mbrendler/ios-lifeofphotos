//
//  LPVacationData.m
//  LifeOfPhotos
//
//  Created by Maik Brendler on 07.08.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import "LPVacationData.h"

NSMutableDictionary *vacations = nil;

@implementation LPVacationData
@synthesize vacation = _vacation;

+ (NSURL*)vacationsUrl
{
    NSFileManager *filemanager = [NSFileManager defaultManager];
    NSURL *documents = [[filemanager
                         URLsForDirectory:NSDocumentDirectory
                         inDomains:NSUserDomainMask] lastObject];
    NSURL *vacationsDirectory = [documents URLByAppendingPathComponent:@"vacations"];
    [filemanager createDirectoryAtURL:vacationsDirectory
          withIntermediateDirectories:YES
                           attributes:nil
                                error:nil];
    return vacationsDirectory;
}

+ (void)openVacation:(NSString *)vacationName
          usingBlock:(completion_block_t)completionBlock
{
    if (!vacations) {
        vacations = [[NSMutableDictionary alloc] init];
    }
    
    UIManagedDocument *vacation = [vacations objectForKey:vacationName];
    if (!vacation) {
        NSURL *vacationUrl = [[self vacationsUrl]
                              URLByAppendingPathComponent:vacationName];
        vacation = [[UIManagedDocument alloc] initWithFileURL:vacationUrl];
        [vacations setObject:vacation forKey:vacationName];
    }
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[vacation.fileURL path]]) {
        [vacation saveToURL:vacation.fileURL
           forSaveOperation:UIDocumentSaveForCreating
          completionHandler:^(BOOL success) {
              completionBlock(vacation);
          }];
    } else if (vacation.documentState == UIDocumentStateClosed) {
        [vacation openWithCompletionHandler:^(BOOL success) {
            completionBlock(vacation);
        }];
    } else if (vacation.documentState == UIDocumentStateNormal) {
        completionBlock(vacation);
    } else {
        NSLog(@"Bad vacation documentState: %d", vacation.documentState);
    }
}

+ (NSArray*)allVacations
{
    NSArray *vacationUrls = [[NSFileManager defaultManager]
                             contentsOfDirectoryAtURL:[self vacationsUrl]
                             includingPropertiesForKeys:nil
                             options:0
                             error:nil];
    NSMutableArray *vacations = [NSMutableArray arrayWithCapacity:[vacationUrls count]];
    for (NSURL *url in vacationUrls) {
        [vacations addObject:[url lastPathComponent]];
    }
    return vacations;
}

@end
