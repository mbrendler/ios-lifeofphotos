//
//  LPRecentPhotosTVC.m
//  LifeOfPhotos
//
//  Created by Maik Brendler on 28.07.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import "LPRecentPhotosTVC.h"
#import "LPPhotoVC.h"

@interface LPRecentPhotosTVC ()

@end

@implementation LPRecentPhotosTVC

-(void)loadTableData
{
    // Do nothing, because table data will be loaded in viewWillAppear
}

- (void)viewWillAppear:(BOOL)animated
{

    self.tableData = [[NSUserDefaults standardUserDefaults]
                      objectForKey:RECENT_PHOTOS_KEY];
}

- (NSString*)cellIdentifier
{
    return @"Recent Photo Table Cell";
}

@end
