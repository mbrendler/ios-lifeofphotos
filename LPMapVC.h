//
//  LPMapVCViewController.h
//  LifeOfPhotos
//
//  Created by Maik Brendler on 31.07.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@class LPMapVC;

@protocol LPMapVCDelegate <NSObject>
@optional
- (UIImage *)mapViewController:(LPMapVC*)sender
            imageForAnnotation:(id <MKAnnotation>)annotation;
- (void)mapViewController:(LPMapVC*)sender
        annotationClicked:(id <MKAnnotation>)annotation;
@end

@interface LPMapVC : UIViewController
@property (nonatomic, strong) NSArray *annotations; // of id <MKAnnotation>
@property (nonatomic, weak) id<LPMapVCDelegate> delegate;
@end
